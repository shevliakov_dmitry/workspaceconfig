set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle
call vundle#begin()

Plugin 'https://github.com/scrooloose/nerdtree.git'
Plugin 'git://github.com/ntpeters/vim-better-whitespace.git'

Plugin 'https://github.com/vim-airline/vim-airline.git'

Plugin 'git://github.com/airblade/vim-gitgutter.git'

Plugin 'https://github.com/Valloric/YouCompleteMe.git'
Plugin 'https://github.com/rdnetto/YCM-Generator.git'

"Syntax highlight
Plugin 'https://github.com/sheerun/vim-polyglot.git'
"Thermes
Plugin 'https://github.com/hewo/vim-colorscheme-deepsea.git'

call vundle#end()

set exrc
filetype plugin indent on
syntax on
set number

set expandtab
set tabstop=4
set shiftwidth=4

"Therme
set background=dark
set t_Co =255
colorscheme deepsea

"VimAirline config
set laststatus=2
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemod=':t'
set timeoutlen=50

"Vim Extra WhiteSpaces
highlight ExtraWhitespace ctermbg=LightBlue

"VimGutter
let g:gitgutter_sign_column_always = 2

"Ycm config
let g:ycm_autoclose_preview_window_after_insertion=1
let g:ycm_confirm_extra_conf=0
let g:ycm_error_symbol = '>E'
let g:ycm_warning_symbol = '>W'
highlight YcmErrorSection cterm=underline
highlight YcmErrorSign ctermfg=Blue
highlight YcmWarningSection cterm=underline
highlight YcmWarningSign ctermfg=LightBlue

"Polyglot
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

"Vim hotkeys
nmap <F4> :make<CR>

"NERDTree hotkeys
nmap <C-n> :NERDTreeToggle<CR>

"YCM hotkeys
nmap <C-g> :YcmCompleter GoTo<CR>
nmap <C-t> :YcmCompleter GetType<CR>
